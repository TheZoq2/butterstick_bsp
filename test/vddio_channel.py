#top = vddio::vddio_channel_duty

from cocotb.clock import Clock
from spade import FallingEdge, SpadeExt
from cocotb import cocotb
from cocotb.triggers import Timer

def v_from_duty(duty: float) -> float:
    return 3.546 - 2.601 * duty

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut)

    dc = 0.2
    for dc in [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
        s.i.target_mv = int(v_from_duty(dc) * 1000)
        await Timer(1, units="ns")
        received = int(s.o.value()) 
        if abs(int(dc * 1024) - received) > 12:
            assert False, f"Out of spec. Got {received} expected {dc * 1024}"
