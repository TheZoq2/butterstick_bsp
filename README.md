# Butterstick Board Support Library

This library provides some utilities for the butterstick board. First, it provides `vddio_driver` which is used to configure the IO voltage for the SYZYGY IO banks on the Butterstick.

Second, it provides a (currently incomplete) LPF file for the board which can
be used by adding the library as a dependency, then setting `pin_file =
"build/deps/butterstick_bsp/butterstick.lpf"

